<?php
$create_table = file_get_contents('../products.sql');
$dbt->exec($create_table);
$st = $dbt->prepare("SELECT * FROM products");
$st->execute();
$i = 0;
$list = array();
while ($exit_info = $st->fetch()) {
    $list[$i]['image'] = $exit_info['image'];
    $list[$i]['name'] = $exit_info['name'];
    $list[$i]['description'] = $exit_info['description'];
    $list[$i]['price'] = $exit_info['price'];
    $i++;
}
$list['counter'] = $i;

