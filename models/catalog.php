<?php

class Creating_Catalog
{
    public function getProductList($st)
    {
        $i = 0;
        $list = array();
        while ($exit_info = $st->fetch()) {
            $list[$i]['image'] = $exit_info['image'];
            $list[$i]['name'] = $exit_info['name'];
            $list[$i]['description'] = $exit_info['description'];
            $list[$i]['price'] = $exit_info['price'];
            $i++;
        }
        $list['counter'] = $i;
        return $list;
    }
}