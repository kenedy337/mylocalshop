<?php
include_once "../config.php";
try {
    $dbt = new PDO("mysql:host=$hostname", $username, $passwordname);
    $dbt->exec("CREATE DATABASE IF NOT EXISTS `$basename`;");
} catch (PDOException $ex) {
    echo 'Connect failed: ' . htmlspecialchars($ex->getMessage());
}
$dbt->exec("USE $basename");
include_once "product.php";
