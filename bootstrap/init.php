<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();
define('__PATH__', dirname(__DIR__). '/');
function __autoload ($class_name) //автоматическая загрузка кслассов
{
   if(is_file(__PATH__. 'models/' . $class_name . '.php')){
       include_once  '__PATH__'. 'models/' . $class_name . '.php';
   }
   elseif(is_file(__PATH__. 'controllers/' . $class_name . '.php')){
      include __PATH__. 'controllers/' . $class_name . '.php';
       return true;
   }
   elseif(is_file(__PATH__. 'app/'. $class_name .'.php')){
       include __PATH__. 'app/'. $class_name. '.php';
   }
   else{
       return false;
   }
}
?>
