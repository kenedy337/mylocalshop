CREATE TABLE IF NOT EXISTS `products` (
`id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `description` text,
  `price` decimal(8,2) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;
INSERT INTO `products` (`id`, `name`, `description`, `price`, `image`) VALUES
(1, 'Мобильный телефон Apple iPhone 5s 16GB Gold (CPO)', 'Экран Retina 4\" (1136x640), емкостный Multi-Touch / моноблок / двухъядерный
Apple <br/>A7 (1.3 ГГц) / ОЗУ 1 ГБ / основная камера 8 Мп, фронтальная 1.2 Мп / Bluetooth <br/>4.0 /Wi-Fi 802.11a/b/g/n / A-GPS / GLONASS /
 16 ГБ встроенной памяти / разъем <br/>3.5 мм / LTE / iOS 8 / 123.8 x 58.6 x 7.6 мм, 112 г / золотой', '9499.00', 'media/img1.jpg'),
(2, 'Мобильный телефон Samsung Galaxy S6 SS 32GB G920 Blue', 'Экран 5.1\" Super AMOLED (2560х1440, сенсорный, емкостный, Multi-Touch) /
<br/>моноблок / Exynos 7420 (Quad 2.1 ГГц + Quad 1.5 ГГц) / камера 16 Мп + фронтальная <br/> 5 Мп // Bluetooth 4.1 / Wi-Fi a/b/g/n/ac /3 ГБ
 оперативной памяти / 32 ГБ <br/> встроенной памяти / разъем 3.5 мм / LTE / GPS / ГЛОНАСС / OC Android 5.0 / 143.4 x <br/>70.5 x 6.8 мм, 138
 г / синий', '15999.00', 'media/img2.jpg'),
(3, 'Мобильный телефон Caterpillar CAT S30 (Dual Sim) Black', 'Экран 4.5\" (854x480, емкостный, Multi-Touch) / моноблок / Qualcomm Snapdragon
 210 <br/>(1.1 ГГц) / 1 ГБ оперативной памяти / камера 5 Мп + фронтальная 2 Мп / Bluetooth <br/>4.1 / Wi-Fi 802.11 b/g/n / 8 ГБ встроенной памяти
  + поддержка microSD <br/>пылевлагозащищённый / поддержка 2-х СИМ-карт / разъем 3.5 мм / 3G / 4G (LTE) / <br/>GPS/ГЛОНАСС / OC Android 5.1',
  '10000.00', 'media/img3.jpg');